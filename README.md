# About the project
The [ZuSkE research project](https://www.ffe.de/projekte/zuske/) supports municipal stakeholders in the joint development of transformation strategies. As an important element of the energy transition, the focus is on measures for sector coupling. Two communication tools were developed in a participatory process with the three practical municipalities of Berlin, Walldorf and Freilassing: this website and a "strategy box" for workshops are intended to support local actors in advancing the transformation process in their municipality. Municipal actors such as grid operators and energy suppliers as well as citizens and supraregional multipliers were involved in the development process. The research results are transferable to other municipalities.

The project was realized by the Institut für Technikfolgenabschätzung (ITAS) of the Karlsruher Institut für Technologie (KIT), the FfE, the Vereinigung Deutscher Wissenschaftler (VdW) with the Zivilgesellschaftliche Plattform Forschungswende and Dialogik and funded by the Bundesministerium für Wirtschaft und Klimaschutz (BMWK) (funding code: 03EI5205A-D).

The website is publicly accessible under the following domain: [https://kommunale-sektorkopplung.ffe.de/](https://kommunale-sektorkopplung.ffe.de/)

In the code provided here, subpages on data protection and imprint have been removed in comparison to the original page, as well as functionalities for tracking website visits.

# Built with
- [Vue2.js](https://v2.vuejs.org/)
- [Vite.js](https://vitejs.dev/)
- [vue-router](https://router.vuejs.org/)
- [vue-meta](https://vue-meta.nuxtjs.org/)
- [bootstrap-vue](https://bootstrap-vue.org/)
- [sass](https://sass-lang.com/)
- [vue-scroll-snap](https://github.com/angelomelonas/vue-scroll-snap)
- [Swiper.js](https://swiperjs.com/)
- [jQuery](https://jquery.com/)
- [node.js](https://nodejs.org/en)

# Installation

1. Install node
2. Clone repository
3. Navigate to the project folder and install packages:
```
npm install 
```

# Starting local server
```
npm run dev
```

Access: [http://localhost:3000](http://localhost:3000)

# Create production build
```
npm run build
```

# Licence
This project is licensed under the MIT License.

# Contact
Timo Limmer ([tlimmer@ffe.de](mailto:tlimmer@ffe.de))
