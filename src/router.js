import Vue from 'vue';
import VueRouter from 'vue-router';
import Meta from 'vue-meta';

// Views
import Home from '@/views/Home.vue';
import Hintergrund from '@/views/HintergrundSektorkopplung.vue';
import KommuneBerlin from '@/views/KommuneBerlin.vue';
import KommuneWalldorf from '@/views/KommuneWalldorf.vue';
import KommuneFreilassing from '@/views/KommuneFreilassing.vue';
import Massnahmenkatalog from '@/views/Massnahmenkatalog.vue';
import Strategiebox from '@/views/Strategiebox.vue';
import ZehnPunktePlan from '@/views/ZehnPunktePlan.vue';
import UeberZuSkE from '@/views/UeberZuSkE.vue';
import Datenschutz from '@/views/Datenschutz.vue';
import Impressum from '@/views/Impressum.vue';


/**
 * @type { import("vue-router").RouteConfig[] }
 */
const routes = [
  { 
    path: '/', 
    component: Home, 
    meta: {isHome: true, isFooterFixed:false} 
  },
  { 
    path: '/hintergrund', 
    component: Hintergrund, 
    meta: {isHome: false, isFooterFixed:true},
    children: [
      { name: 'deutschlands-klimaziel', path: '/hintergrund#deutschlands-klimaziel'},
      { name: 'status-quo', path: '/hintergrund#status-quo'},
      { name: 'strom-entwicklung', path: '/hintergrund#strom-entwicklung'},
      { name: 'sektorkopplung-definition', path: '/hintergrund#sektorkopplung-definition'},
      { name: 'power-to-x', path: '/hintergrund#power-to-x'},
      { name: 'mehrwert', path: '/hintergrund#mehrwert'},
      { name: 'kommunale-ebene', path: '/hintergrund#kommunale-ebene'},
      { name: 'rollen-kommune', path: '/hintergrund#rollen-kommune'},
      { name: 'umsetzung', path: '/hintergrund#umsetzung'},
    ]
  },
  { path: '/berlin', component: KommuneBerlin, meta: {isHome: false, isFooterFixed:true}  },
  { path: '/walldorf', component: KommuneWalldorf, meta: {isHome: false, isFooterFixed:true}  },
  { path: '/freilassing', component: KommuneFreilassing, meta: {isHome: false, isFooterFixed:true}  },
  { path: '/massnahmenkatalog', component: Massnahmenkatalog, meta: {isHome: false, isFooterFixed:false}  },
  { path: '/strategiebox', component: Strategiebox, meta: {isHome: false, isFooterFixed:false}  },
  { path: '/10-punkte-plan', component: ZehnPunktePlan, meta: {isHome: false, isFooterFixed:false}  },
  { path: '/ueber-zuske', component: UeberZuSkE, meta: {isHome: false, isFooterFixed:false}  },
  { path: '/datenschutz', component: Datenschutz, meta: {isHome: false, isFooterFixed:false}  },
  { path: '/impressum', component: Impressum, meta: {isHome: false, isFooterFixed:false}  },
  { path: '/:pathMatch(.*)*', component: Home },
];

const scrollBehavior = async function (to, from, savedPosition) {
  if (savedPosition) {
    return savedPosition;
  }
  else {
    const position = {};

    if (to.params.scroll == 'false') {

      //console.log(position)
      return position
    }

    if (to.hash=='#deutschlands-klimaziel' || to.hash=='#umsetzung' || to.hash=='#status_quo'  ||  to.hash=='#scenarios' || to.hash=='#model_landscape' || to.hash=='#findings' ) {
      /*if (to.meta == from.meta) {
        position.behavior = "smooth" //bei Bedarf auskommentieren --> Unterseiten Bug skigle
        
      }*/
      position.selector = to.hash;

      //console.log("Scroll behavior" + to.hash)


      if (document.querySelector(to.hash)) {
        //console.log("Scroll behavior" + to.hash)
        return position;
      }
    }
    else{
      //document.getElementById('top').scrollIntoView();
      //document.getElementById('app').scrollIntoView();
      //console.log("Scroll behavior" + to.hash)
    }
  }

  /*if (to.hash) {
    return {
      selector: to.hash
    }
  }
  return { x: 0, y: 0 };*/
};

//ScrollBehavior von XOS --> Fehler
/*const scrollBehavior = function (to, from, savedPosition) {
  
  if (savedPosition) {
    return savedPosition
  }

  
  else {
    const position = {};

    if (to.params.scroll == 'false') {

      //console.log(position)
      return position
    }

    if (to.hash=='#start' || to.hash=='#deutschlands-klimaziel' || to.hash=='#status_quo'  ||  to.hash=='#strom-enticklung' ) {
      if (to.meta == from.meta) {
        position.behavior = "smooth" //bei Bedarf auskommentieren --> Unterseiten Bug skigle
        
      }
      position.selector = to.hash;

      //console.log("Scroll behavior" + to.hash)


      if (document.querySelector(to.hash)) {
        //console.log("Scroll behavior" + to.hash)
        return position;
      }
    }
    else{
      document.getElementById('top').scrollIntoView();
    }
  }
};*/

Vue.use(VueRouter);
Vue.use(Meta);

export default new VueRouter({
  mode: 'history',
  routes,
  //scrollBehavior
  scrollBehavior(to, from, savedPosition) {
    //console.log('Scrollbehavior')
    console.log(savedPosition);
    if (to.hash) {
        console.log('Scrollbehavior Hash:' + to.hash)
        return { selector: to.hash }
    } else if (savedPosition) {
        console.log('Scrollbehavior savedPosition:' + savedPosition)
        return savedPosition;
    } else {
        console.log('Scrollbehavior Fallback')
        return { x: 0, y: 0 }
    }
  }
});
